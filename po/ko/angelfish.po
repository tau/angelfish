# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-angelfish package.
# Jung Hee Lee <daemul72@gmail.com>, 2019.
# Shinjo Park <kde@peremen.name>, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-angelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-10 00:45+0000\n"
"PO-Revision-Date: 2023-04-21 23:28+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "박신조"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde@peremen.name"

#: angelfish-webapp/main.cpp:86
#, kde-format
msgid "desktop file to open"
msgstr "열 데스크톱 파일"

#: angelfish-webapp/main.cpp:113
#, kde-format
msgid "Angelfish Web App runtime"
msgstr "Angelfish 웹 앱 런타임"

#: angelfish-webapp/main.cpp:115
#, kde-format
msgid "Copyright 2020 Angelfish developers"
msgstr "Copyright 2020 Angelfish developers"

#: angelfish-webapp/main.cpp:117
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: lib/angelfishwebprofile.cpp:63
#, kde-format
msgid "Download finished"
msgstr "다운로드 완료"

#: lib/angelfishwebprofile.cpp:67
#, kde-format
msgid "Download failed"
msgstr "다운로드 실패"

#: lib/contents/ui/AuthSheet.qml:17
#, kde-format
msgid "Authentication required"
msgstr "인증 필요함"

#: lib/contents/ui/AuthSheet.qml:30
#, kde-format
msgid "Username"
msgstr "사용자 이름"

#: lib/contents/ui/AuthSheet.qml:37
#, kde-format
msgid "Password"
msgstr "암호"

#: lib/contents/ui/DownloadQuestion.qml:12
#, kde-format
msgid "Do you want to download this file?"
msgstr "이 파일을 다운로드 하시겠습니까?"

#: lib/contents/ui/DownloadQuestion.qml:20
#: src/contents/ui/AdblockFilterDownloadQuestion.qml:29
#, kde-format
msgid "Download"
msgstr "다운로드"

#: lib/contents/ui/DownloadQuestion.qml:28 lib/contents/ui/PrintPreview.qml:167
#: src/contents/ui/Downloads.qml:41
#, kde-format
msgid "Cancel"
msgstr "취소"

#: lib/contents/ui/ErrorHandler.qml:47
#, kde-format
msgid "Error loading the page"
msgstr "페이지를 불러오는 중 오류 발생"

#: lib/contents/ui/ErrorHandler.qml:58
#, kde-format
msgid "Retry"
msgstr "다시 시도"

#: lib/contents/ui/ErrorHandler.qml:71
#, kde-format
msgid ""
"Do you wish to continue?\n"
"\n"
" If you wish so, you may continue with an unverified certificate.\n"
" Accepting an unverified certificate means\n"
" you may not be connected with the host you tried to connect to.\n"
" Do you wish to override the security check and continue?"
msgstr ""
"계속 진행하시겠습니까?\n"
"\n"
" 계속 진행하면 검증되지 않은 인증서를 사용합니다.\n"
" 검증되지 않은 인증서를 사용한다면\n"
" 연결하려고 했던 호스트에 연결되지 않을 수도 있습니다.\n"
" 보안 검사를 무시하고 계속 진행하시겠습니까?"

#: lib/contents/ui/ErrorHandler.qml:79
#, kde-format
msgid "Yes"
msgstr "예"

#: lib/contents/ui/ErrorHandler.qml:88
#, kde-format
msgid "No"
msgstr "아니요"

#: lib/contents/ui/JavaScriptDialogSheet.qml:49
#, kde-format
msgid "This page says"
msgstr "페이지 프롬프트"

#: lib/contents/ui/JavaScriptDialogSheet.qml:64
#, kde-format
msgid ""
"The website asks for confirmation that you want to leave. Unsaved "
"information might not be saved."
msgstr ""
"웹사이트를 떠날지 물어 보고 있습니다. 저장하지 않은 정보는 손실될 수 있습니"
"다."

#: lib/contents/ui/JavaScriptDialogSheet.qml:79
#, kde-format
msgid "Leave page"
msgstr "페이지 떠나기"

#: lib/contents/ui/JavaScriptDialogSheet.qml:87
#, kde-format
msgid "Submit"
msgstr "보내기"

#: lib/contents/ui/PermissionQuestion.qml:16
#, kde-format
msgid "Do you want to allow the website to access the geo location?"
msgstr "웹사이트에서 현재 위치에 접근할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:19
#, kde-format
msgid "Do you want to allow the website to access the microphone?"
msgstr "웹사이트에서 마이크를 사용할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:22
#, kde-format
msgid "Do you want to allow the website to access the camera?"
msgstr "웹사이트에서 카메라를 사용할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:25
#, kde-format
msgid ""
"Do you want to allow the website to access the camera and the microphone?"
msgstr "웹사이트에서 카메라와 마이크를 사용할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:28
#, kde-format
msgid "Do you want to allow the website to share your screen?"
msgstr "웹사이트에서 화면을 공유할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:31
#, kde-format
msgid "Do you want to allow the website to share the sound output?"
msgstr "웹사이트에서 소리 출력을 공유할 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:34
#, kde-format
msgid "Do you want to allow the website to send you notifications?"
msgstr "웹사이트에서 알림을 보낼 수 있도록 허용하시겠습니까?"

#: lib/contents/ui/PermissionQuestion.qml:43
#, kde-format
msgid "Accept"
msgstr "수락"

#: lib/contents/ui/PermissionQuestion.qml:53
#, kde-format
msgid "Decline"
msgstr "거절"

#: lib/contents/ui/PrintPreview.qml:24 src/contents/ui/desktop/desktop.qml:269
#, kde-format
msgid "Print"
msgstr "인쇄"

#: lib/contents/ui/PrintPreview.qml:67
#, kde-format
msgid "Destination"
msgstr "대상"

#: lib/contents/ui/PrintPreview.qml:71
#, kde-format
msgid "Save to PDF"
msgstr "PDF로 저장"

#: lib/contents/ui/PrintPreview.qml:77
#, kde-format
msgid "Orientation"
msgstr "방향"

#: lib/contents/ui/PrintPreview.qml:102
#, kde-format
msgid "Paper size"
msgstr "용지 크기"

#: lib/contents/ui/PrintPreview.qml:148
#, kde-format
msgid "Options"
msgstr "옵션"

#: lib/contents/ui/PrintPreview.qml:152
#, kde-format
msgid "Print backgrounds"
msgstr "배경 인쇄"

#: lib/contents/ui/PrintPreview.qml:172
#, kde-format
msgid "Save"
msgstr "저장"

#: lib/contents/ui/WebView.qml:235
#, kde-format
msgid "Website was opened in a new tab"
msgstr "웹사이트가 새 탭에서 열렸습니다."

#: lib/contents/ui/WebView.qml:253
#, kde-format
msgid "Entered Full Screen Mode"
msgstr "전체 화면 모드에 진입함"

#: lib/contents/ui/WebView.qml:254
#, kde-format
msgid "Exit Full Screen (Esc)"
msgstr "전체 화면 끝내기(Esc)"

#: lib/contents/ui/WebView.qml:387
#, kde-format
msgid "Play"
msgstr "재생"

#: lib/contents/ui/WebView.qml:388 src/contents/ui/Downloads.qml:47
#, kde-format
msgid "Pause"
msgstr "일시 정지"

#: lib/contents/ui/WebView.qml:395
#, kde-format
msgid "Unmute"
msgstr "음소거 해제"

#: lib/contents/ui/WebView.qml:396
#, kde-format
msgid "Mute"
msgstr "음소거"

#: lib/contents/ui/WebView.qml:406
#, kde-format
msgid "Speed"
msgstr "속도"

#: lib/contents/ui/WebView.qml:429
#, kde-format
msgid "Loop"
msgstr "반복"

#: lib/contents/ui/WebView.qml:436
#, kde-format
msgid "Exit fullscreen"
msgstr "전체 화면 모드 끝내기"

#: lib/contents/ui/WebView.qml:436
#, kde-format
msgid "Fullscreen"
msgstr "전체 화면"

#: lib/contents/ui/WebView.qml:449
#, kde-format
msgid "Hide controls"
msgstr "컨트롤 숨기기"

#: lib/contents/ui/WebView.qml:450
#, kde-format
msgid "Show controls"
msgstr "컨트롤 표시"

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgid "Open video"
msgstr "비디오 열기"

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgid "Open audio"
msgstr "오디오 열기"

#: lib/contents/ui/WebView.qml:459
#, kde-format
msgid "Open video in new Tab"
msgstr "새 탭에서 비디오 열기"

#: lib/contents/ui/WebView.qml:459
#, kde-format
msgid "Open audio in new Tab"
msgstr "새 탭에서 오디오 열기"

#: lib/contents/ui/WebView.qml:471
#, kde-format
msgid "Save video"
msgstr "비디오 저장"

#: lib/contents/ui/WebView.qml:477
#, kde-format
msgid "Copy video Link"
msgstr "비디오 링크 복사"

#: lib/contents/ui/WebView.qml:483
#, kde-format
msgid "Open image"
msgstr "그림 열기"

#: lib/contents/ui/WebView.qml:483
#, kde-format
msgid "Open image in new Tab"
msgstr "새 탭에서 그림 열기"

#: lib/contents/ui/WebView.qml:495
#, kde-format
msgid "Save image"
msgstr "그림 저장"

#: lib/contents/ui/WebView.qml:501
#, kde-format
msgid "Copy image"
msgstr "그림 복사"

#: lib/contents/ui/WebView.qml:507
#, kde-format
msgid "Copy image link"
msgstr "그림 링크 복사"

#: lib/contents/ui/WebView.qml:513
#, kde-format
msgid "Open link"
msgstr "링크 열기"

#: lib/contents/ui/WebView.qml:513
#, kde-format
msgid "Open link in new Tab"
msgstr "새 탭으로 링크 열기"

#: lib/contents/ui/WebView.qml:525
#, kde-format
msgid "Bookmark link"
msgstr "링크를 책갈피에 추가"

#: lib/contents/ui/WebView.qml:537
#, kde-format
msgid "Save link"
msgstr "링크 저장"

#: lib/contents/ui/WebView.qml:543
#, kde-format
msgid "Copy link"
msgstr "링크 복사"

#: lib/contents/ui/WebView.qml:550
#, kde-format
msgid "Copy"
msgstr "복사"

#: lib/contents/ui/WebView.qml:556
#, kde-format
msgid "Cut"
msgstr "잘라내기"

#: lib/contents/ui/WebView.qml:562
#, kde-format
msgid "Paste"
msgstr "붙여넣기"

#: lib/contents/ui/WebView.qml:583 src/contents/ui/desktop/desktop.qml:318
#: src/contents/ui/mobile.qml:280
#, kde-format
msgid "Share page"
msgstr "공유 페이지"

#: lib/contents/ui/WebView.qml:595
#, kde-format
msgid "View page source"
msgstr "페이지 소스 보기"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:14
#, kde-format
msgid ""
"The ad blocker is missing its filter lists, do you want to download them now?"
msgstr "광고 차단기 필터가 비어 있습니다. 지금 다운로드하시겠습니까?"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:34
#, kde-format
msgid "Downloading..."
msgstr "다운로드 중..."

#: src/contents/ui/Bookmarks.qml:12 src/contents/ui/desktop/desktop.qml:248
#: src/contents/ui/mobile.qml:79
#, kde-format
msgid "Bookmarks"
msgstr "책갈피"

#: src/contents/ui/desktop/BookmarksPage.qml:28
#, kde-format
msgid "Search bookmarks…"
msgstr "책갈피 검색…"

#: src/contents/ui/desktop/BookmarksPage.qml:70
#, kde-format
msgid "No bookmarks yet"
msgstr "책갈피 없음"

#: src/contents/ui/desktop/desktop.qml:116
#, kde-format
msgid "Search or enter URL…"
msgstr "검색 또는 URL 입력…"

#: src/contents/ui/desktop/desktop.qml:223 src/contents/ui/desktop/Tabs.qml:354
#: src/contents/ui/Tabs.qml:49
#, kde-format
msgid "New Tab"
msgstr "새 탭"

#: src/contents/ui/desktop/desktop.qml:230 src/contents/ui/mobile.qml:71
#, kde-format
msgid "Leave private mode"
msgstr "사생활 보호 모드 나가기"

#: src/contents/ui/desktop/desktop.qml:230 src/contents/ui/mobile.qml:71
#, kde-format
msgid "Private mode"
msgstr "사생활 보호 모드"

#: src/contents/ui/desktop/desktop.qml:239 src/contents/ui/History.qml:12
#: src/contents/ui/mobile.qml:87
#, kde-format
msgid "History"
msgstr "과거 기록"

#: src/contents/ui/desktop/desktop.qml:257 src/contents/ui/Downloads.qml:15
#: src/contents/ui/mobile.qml:91
#, kde-format
msgid "Downloads"
msgstr "다운로드"

#: src/contents/ui/desktop/desktop.qml:275
#, kde-format
msgid "Full Screen"
msgstr "전체 화면"

#: src/contents/ui/desktop/desktop.qml:292
#, kde-format
msgid "Hide developer tools"
msgstr "개발자 도구 숨기기"

#: src/contents/ui/desktop/desktop.qml:293 src/contents/ui/mobile.qml:376
#, kde-format
msgid "Show developer tools"
msgstr "개발자 도구 표시"

#: src/contents/ui/desktop/desktop.qml:302 src/contents/ui/mobile.qml:276
#, kde-format
msgid "Find in page"
msgstr "페이지에서 찾기"

#: src/contents/ui/desktop/desktop.qml:311
#, kde-format
msgctxt "@action:inmenu"
msgid "Reader Mode"
msgstr "읽기 모드"

#: src/contents/ui/desktop/desktop.qml:328
#, kde-format
msgid "Add to application launcher"
msgstr "앱 실행기에 추가"

#: src/contents/ui/desktop/desktop.qml:347 src/contents/ui/mobile.qml:99
#, kde-format
msgid "Settings"
msgstr "설정"

#: src/contents/ui/desktop/desktop.qml:354
#, kde-format
msgid "Configure Angelfish"
msgstr "Angelfish 설정"

#: src/contents/ui/desktop/HistoryPage.qml:28
#, kde-format
msgid "Search history…"
msgstr "과거 기록 검색…"

#: src/contents/ui/desktop/HistoryPage.qml:44
#, kde-format
msgid "Clear all history"
msgstr "모든 기록 삭제"

#: src/contents/ui/desktop/HistoryPage.qml:80
#, kde-format
msgid "Not history yet"
msgstr "과거 기록이 비어 있음"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:14
#: src/contents/ui/SettingsPage.qml:38
#, kde-format
msgid "Toolbars"
msgstr "도구 모음"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:28
#, kde-format
msgid "Show home button:"
msgstr "홈 단추 표시:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:29
#, kde-format
msgid "The home button will be shown next to the reload button in the toolbar."
msgstr "도구 모음의 새로 고침 단추 옆에 홈 단추를 표시합니다."

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:39
#, kde-format
msgid "Homepage:"
msgstr "홈페이지:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:61
#, kde-format
msgid "New tabs:"
msgstr "새 탭:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:83
#, kde-format
msgid "Always show the tab bar"
msgstr "항상 탭 목록 표시"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:84
#, kde-format
msgid "The tab bar will be displayed even if there is only one tab open"
msgstr "탭이 하나만 열려 있어도 탭 표시줄이 표시됨"

#: src/contents/ui/desktop/Tabs.qml:219 src/contents/ui/Tabs.qml:185
#, kde-format
msgid "Reader Mode: %1"
msgstr "읽기 모드: %1"

#: src/contents/ui/desktop/Tabs.qml:250 src/contents/ui/Tabs.qml:216
#, kde-format
msgid "Close tab"
msgstr "탭 닫기"

#: src/contents/ui/desktop/Tabs.qml:290
#, kde-format
msgid "Open a new tab"
msgstr "새 탭 열기"

#: src/contents/ui/desktop/Tabs.qml:323
#, kde-format
msgid "List all tabs"
msgstr "모든 탭 표시"

#: src/contents/ui/desktop/Tabs.qml:363
#, kde-format
msgid "Reload Tab"
msgstr "탭 새로 고침"

#: src/contents/ui/desktop/Tabs.qml:371
#, kde-format
msgid "Duplicate Tab"
msgstr "탭 복제"

#: src/contents/ui/desktop/Tabs.qml:379
#, kde-format
msgid "Bookmark Tab"
msgstr "책갈피에 탭 추가"

#: src/contents/ui/desktop/Tabs.qml:395
#, kde-format
msgid "Close Tab"
msgstr "탭 닫기"

#: src/contents/ui/Downloads.qml:26
#, kde-format
msgid "No running downloads"
msgstr "실행 중인 다운로드 없음"

#: src/contents/ui/Downloads.qml:53
#, kde-format
msgid "Continue"
msgstr "계속"

#: src/contents/ui/Downloads.qml:91
#, kde-format
msgctxt "download state"
msgid "Starting…"
msgstr "시작 중..."

#: src/contents/ui/Downloads.qml:93
#, kde-format
msgid "Completed"
msgstr "완료됨"

#: src/contents/ui/Downloads.qml:95
#, kde-format
msgid "Cancelled"
msgstr "취소됨"

#: src/contents/ui/Downloads.qml:97
#, kde-format
msgctxt "download state"
msgid "Interrupted"
msgstr "중지됨"

#: src/contents/ui/Downloads.qml:99
#, kde-format
msgctxt "download state"
msgid "In progress"
msgstr "진행 중"

#: src/contents/ui/FindInPageBar.qml:45
#, kde-format
msgid "Search..."
msgstr "검색..."

#: src/contents/ui/mobile.qml:18
#, kde-format
msgid "Angelfish Web Browser"
msgstr "Angelfish 웹 브라우저"

#: src/contents/ui/mobile.qml:64 src/contents/ui/Tabs.qml:42
#, kde-format
msgid "Tabs"
msgstr "탭"

#: src/contents/ui/mobile.qml:291
#, kde-format
msgid "Add to homescreen"
msgstr "홈 화면에 추가"

#: src/contents/ui/mobile.qml:301
#, kde-format
msgid "Open in app"
msgstr "앱에서 열기"

#: src/contents/ui/mobile.qml:309
#, kde-format
msgid "Go previous"
msgstr "뒤로 이동"

#: src/contents/ui/mobile.qml:317
#: src/contents/ui/SettingsNavigationBarPage.qml:76
#, kde-format
msgid "Go forward"
msgstr "앞으로 이동"

#: src/contents/ui/mobile.qml:324
#, kde-format
msgid "Stop loading"
msgstr "불러오기 멈추기"

#: src/contents/ui/mobile.qml:324
#, kde-format
msgid "Refresh"
msgstr "새로 고침"

#: src/contents/ui/mobile.qml:334
#, kde-format
msgid "Bookmarked"
msgstr "책갈피에 추가됨"

#: src/contents/ui/mobile.qml:334
#, kde-format
msgid "Bookmark"
msgstr "책갈피"

#: src/contents/ui/mobile.qml:350
#, kde-format
msgid "Show desktop site"
msgstr "데스크톱 사이트 표시"

#: src/contents/ui/mobile.qml:359
#, kde-format
msgid "Reader Mode"
msgstr "읽기 모드"

#: src/contents/ui/mobile.qml:367
#, kde-format
msgid "Hide navigation bar"
msgstr "탐색 표시줄 숨기기"

#: src/contents/ui/mobile.qml:367
#, kde-format
msgid "Show navigation bar"
msgstr "탐색 표시줄 표시"

#: src/contents/ui/NewTabQuestion.qml:11
#, kde-format
msgid ""
"Site wants to open a new tab: \n"
"%1"
msgstr ""
"사이트에서 새 탭을 열려고 합니다: \n"
"%1"

#: src/contents/ui/NewTabQuestion.qml:19
#, kde-format
msgid "Open"
msgstr "열기"

#: src/contents/ui/SettingsAdblock.qml:17
#, kde-format
msgid "Adblock settings"
msgstr "광고 차단 설정"

#: src/contents/ui/SettingsAdblock.qml:28
#, kde-format
msgid "Update lists"
msgstr "목록 업데이트"

#: src/contents/ui/SettingsAdblock.qml:50
#, kde-format
msgid "The adblock functionality isn't included in this build."
msgstr "이 빌드에 광고 차단 기능이 제외되어 있습니다."

#: src/contents/ui/SettingsAdblock.qml:56
#, kde-format
msgid "Add filterlist"
msgstr "필터 목록 추가"

#: src/contents/ui/SettingsAdblock.qml:61
#, kde-format
msgid "Name"
msgstr "이름"

#: src/contents/ui/SettingsAdblock.qml:70
#, kde-format
msgid "Url"
msgstr "URL"

#: src/contents/ui/SettingsAdblock.qml:80
#, kde-format
msgid "Add"
msgstr "추가"

#: src/contents/ui/SettingsAdblock.qml:145
#, kde-format
msgid "Remove this filter list"
msgstr "이 필터 목록 삭제"

#: src/contents/ui/SettingsAdblock.qml:155
#, kde-format
msgid "add Filterlist"
msgstr "필터 목록 추가"

#: src/contents/ui/SettingsGeneral.qml:17 src/contents/ui/SettingsPage.qml:14
#, kde-format
msgid "General"
msgstr "일반"

#: src/contents/ui/SettingsGeneral.qml:40
#, kde-format
msgid "Enable JavaScript"
msgstr "자바스크립트 사용"

#: src/contents/ui/SettingsGeneral.qml:41
#, kde-format
msgid "This may be required on certain websites for them to work."
msgstr "일부 웹사이트에서 필요할 수도 있습니다."

#: src/contents/ui/SettingsGeneral.qml:50
#, kde-format
msgid "Load images"
msgstr "그림 불러오기"

#: src/contents/ui/SettingsGeneral.qml:51
#, kde-format
msgid "Whether to load images on websites."
msgstr "웹사이트의 그림을 불러올지 여부입니다."

#: src/contents/ui/SettingsGeneral.qml:60
#, kde-format
msgid "Enable adblock"
msgstr "광고 차단 활성화"

#: src/contents/ui/SettingsGeneral.qml:61
#, kde-format
msgid "Attempts to prevent advertisements on websites from showing."
msgstr "웹사이트에 표시되는 광고를 차단합니다."

#: src/contents/ui/SettingsGeneral.qml:61
#, kde-format
msgid "AdBlock functionality was not included in this build."
msgstr "이 빌드에 광고 차단 기능이 제외되어 있습니다."

#: src/contents/ui/SettingsGeneral.qml:71
#, kde-format
msgid "Switch to new tab immediately"
msgstr "새 탭으로 바로 전환"

#: src/contents/ui/SettingsGeneral.qml:72
#, kde-format
msgid ""
"When you open a link, image or media in a new tab, switch to it immediately"
msgstr "링크, 그림, 미디어를 새 탭으로 열 때 연 즉시 전환"

#: src/contents/ui/SettingsGeneral.qml:81
#, kde-format
msgid "Use Smooth Scrolling"
msgstr "부드러운 스크롤 사용"

#: src/contents/ui/SettingsGeneral.qml:82
#, kde-format
msgid ""
"Scrolling is smoother and will not stop suddenly when you stop scrolling. "
"Requires app restart to take effect."
msgstr ""
"스크롤이 더 부드럽게 진행되며 스크롤을 중단했을 때 갑자기 중단되지 않습니다."
"이 옵션을 변경하려면 다시 시작해야 합니다."

#: src/contents/ui/SettingsGeneral.qml:91
#, kde-format
msgid "Use dark color scheme"
msgstr "어두운 색 배열 사용"

#: src/contents/ui/SettingsGeneral.qml:92
#, kde-format
msgid ""
"Websites will have their color schemes set to dark. Requires app restart to "
"take effect."
msgstr ""
"웹사이트에서 어두운 색 배열을 사용하도록 설정합니다. 이 옵션을 변경하려면 다"
"시 시작해야 합니다."

#: src/contents/ui/SettingsNavigationBarPage.qml:16
#, kde-format
msgid "Navigation bar"
msgstr "탐색 표시줄"

#: src/contents/ui/SettingsNavigationBarPage.qml:36
#, kde-format
msgid ""
"Choose the buttons enabled in navigation bar. Some of the buttons can be "
"hidden only in portrait orientation of the browser and are always shown if  "
"the browser is wider than its height.\n"
"\n"
" Note that if you disable the menu buttons, you will be able to access the "
"menus either by swiping from the left or right side or to a side along the "
"bottom of the window."
msgstr ""
"탐색 표시줄에 활성화된 단추를 선택하십시오. 일부 단추는 브라우저가 가로 모드"
"일 때 숨겨지며 브라우저 너비가 높이보다 클 때 항상 표시됩니다.\n"
"\n"
" 메뉴 단추를 비활성화하면 화면 왼쪽이나 오른쪽에서 밀거나 창 아래쪽에서 밀어"
"서 메뉴에 접근할 수 있습니다."

#: src/contents/ui/SettingsNavigationBarPage.qml:52
#, kde-format
msgid "Main menu in portrait"
msgstr "세로로 주 메뉴 표시"

#: src/contents/ui/SettingsNavigationBarPage.qml:58
#, kde-format
msgid "Tabs in portrait"
msgstr "세로로 탭 표시"

#: src/contents/ui/SettingsNavigationBarPage.qml:64
#, kde-format
msgid "Context menu in portrait"
msgstr "세로로 콘텍스트 메뉴 표시"

#: src/contents/ui/SettingsNavigationBarPage.qml:70
#, kde-format
msgid "Go back"
msgstr "뒤로 이동"

#: src/contents/ui/SettingsNavigationBarPage.qml:82
#, kde-format
msgid "Reload/Stop"
msgstr "새로 고침/중지"

#: src/contents/ui/SettingsPage.qml:20
#, kde-format
msgid "Ad Block"
msgstr "광고 차단"

#: src/contents/ui/SettingsPage.qml:26 src/contents/ui/SettingsWebApps.qml:16
#, kde-format
msgid "Web Apps"
msgstr "웹 앱"

#: src/contents/ui/SettingsPage.qml:32
#: src/contents/ui/SettingsSearchEnginePage.qml:16
#: src/contents/ui/SettingsSearchEnginePage.qml:122
#, kde-format
msgid "Search Engine"
msgstr "검색 엔진"

#: src/contents/ui/SettingsSearchEnginePage.qml:32
#, kde-format
msgid "Custom"
msgstr "사용자 정의"

#: src/contents/ui/SettingsSearchEnginePage.qml:123
#, kde-format
msgid "Base URL of your preferred search engine"
msgstr "선호하는 검색 엔진의 기본 URL"

#: src/contents/ui/SettingsWebApps.qml:75
#, kde-format
msgid "Remove app"
msgstr "앱 삭제"

#: src/contents/ui/ShareSheet.qml:17
#, kde-format
msgid "Share page to"
msgstr "다음으로 페이지 공유"

#: src/main.cpp:95
#, kde-format
msgid "URL to open"
msgstr "열려는 URL"

#, fuzzy
#~| msgid "Private mode"
#~ msgid "Private Mode"
#~ msgstr "사생활 보호 모드"

#~ msgid "Enabled"
#~ msgstr "사용함"

#~ msgid "Disabled"
#~ msgstr "사용 안 함"

#~ msgid "Search online for '%1'"
#~ msgstr "온라인에서 '%1' 검색"

#~ msgid "Search online"
#~ msgstr "온라인에서 검색"

#~ msgid "Confirm"
#~ msgstr "확인"

#~ msgid "OK"
#~ msgstr "확인"

#~ msgid "Enable Adblock"
#~ msgstr "광고 차단 활성화"

#, fuzzy
#~| msgid "Homepage"
#~ msgid "Home"
#~ msgstr "홈페이지"

#~ msgid "Adblock filter lists"
#~ msgstr "광고 차단 필터 목록"

#~ msgid "New"
#~ msgstr "새로 만들기"

#~ msgid "Choose the buttons enabled in navigation bar. "
#~ msgstr "탐색 표시줄에 표시할 단추를 선택하십시오."

#~ msgid "Website that should be loaded on startup"
#~ msgstr "시작 시 불러올 웹 사이트"

#~ msgid "Find..."
#~ msgstr "찾기..."

#~ msgid "Highlight text on the current website"
#~ msgstr "현재 웹 사이트에서 텍스트 강조하기"

#~ msgid "Start without UI"
#~ msgstr "UI 없이 시작"
