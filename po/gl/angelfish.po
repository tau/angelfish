# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-angelfish package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-angelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-10 00:45+0000\n"
"PO-Revision-Date: 2020-03-07 18:15+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: angelfish-webapp/main.cpp:86
#, kde-format
msgid "desktop file to open"
msgstr ""

#: angelfish-webapp/main.cpp:113
#, fuzzy, kde-format
#| msgid "Angelfish Web Browser"
msgid "Angelfish Web App runtime"
msgstr "Navegador web Angelfish"

#: angelfish-webapp/main.cpp:115
#, kde-format
msgid "Copyright 2020 Angelfish developers"
msgstr ""

#: angelfish-webapp/main.cpp:117
#, kde-format
msgid "Marco Martin"
msgstr ""

#: lib/angelfishwebprofile.cpp:63
#, kde-format
msgid "Download finished"
msgstr "Rematou a descarga"

#: lib/angelfishwebprofile.cpp:67
#, kde-format
msgid "Download failed"
msgstr "A descarga fallou"

#: lib/contents/ui/AuthSheet.qml:17
#, kde-format
msgid "Authentication required"
msgstr "Debe autenticarse"

#: lib/contents/ui/AuthSheet.qml:30
#, kde-format
msgid "Username"
msgstr "Nome de usuario"

#: lib/contents/ui/AuthSheet.qml:37
#, kde-format
msgid "Password"
msgstr "Contrasinal"

#: lib/contents/ui/DownloadQuestion.qml:12
#, kde-format
msgid "Do you want to download this file?"
msgstr "Quere descargar este ficheiro?"

#: lib/contents/ui/DownloadQuestion.qml:20
#: src/contents/ui/AdblockFilterDownloadQuestion.qml:29
#, kde-format
msgid "Download"
msgstr "Descargar"

#: lib/contents/ui/DownloadQuestion.qml:28 lib/contents/ui/PrintPreview.qml:167
#: src/contents/ui/Downloads.qml:41
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: lib/contents/ui/ErrorHandler.qml:47
#, kde-format
msgid "Error loading the page"
msgstr "Erro ao cargar a páxina"

#: lib/contents/ui/ErrorHandler.qml:58
#, kde-format
msgid "Retry"
msgstr ""

#: lib/contents/ui/ErrorHandler.qml:71
#, kde-format
msgid ""
"Do you wish to continue?\n"
"\n"
" If you wish so, you may continue with an unverified certificate.\n"
" Accepting an unverified certificate means\n"
" you may not be connected with the host you tried to connect to.\n"
" Do you wish to override the security check and continue?"
msgstr ""

#: lib/contents/ui/ErrorHandler.qml:79
#, kde-format
msgid "Yes"
msgstr ""

#: lib/contents/ui/ErrorHandler.qml:88
#, kde-format
msgid "No"
msgstr ""

#: lib/contents/ui/JavaScriptDialogSheet.qml:49
#, kde-format
msgid "This page says"
msgstr ""

#: lib/contents/ui/JavaScriptDialogSheet.qml:64
#, kde-format
msgid ""
"The website asks for confirmation that you want to leave. Unsaved "
"information might not be saved."
msgstr ""

#: lib/contents/ui/JavaScriptDialogSheet.qml:79
#, fuzzy, kde-format
#| msgid "Share page"
msgid "Leave page"
msgstr "Compartir a páxina"

#: lib/contents/ui/JavaScriptDialogSheet.qml:87
#, kde-format
msgid "Submit"
msgstr ""

#: lib/contents/ui/PermissionQuestion.qml:16
#, kde-format
msgid "Do you want to allow the website to access the geo location?"
msgstr "Quere permitir que o sitio web acceda á posición xeográfica?"

#: lib/contents/ui/PermissionQuestion.qml:19
#, kde-format
msgid "Do you want to allow the website to access the microphone?"
msgstr "Quere permitir que o sitio web acceda ao micrófono?"

#: lib/contents/ui/PermissionQuestion.qml:22
#, kde-format
msgid "Do you want to allow the website to access the camera?"
msgstr "Quere permitir que o sitio web acceda á cámara?"

#: lib/contents/ui/PermissionQuestion.qml:25
#, kde-format
msgid ""
"Do you want to allow the website to access the camera and the microphone?"
msgstr "Quere permitir que o sitio web acceda á cámara e ao micrófono?"

#: lib/contents/ui/PermissionQuestion.qml:28
#, fuzzy, kde-format
#| msgid "Do you want to allow the website to access the camera?"
msgid "Do you want to allow the website to share your screen?"
msgstr "Quere permitir que o sitio web acceda á cámara?"

#: lib/contents/ui/PermissionQuestion.qml:31
#, fuzzy, kde-format
#| msgid "Do you want to allow the website to access the geo location?"
msgid "Do you want to allow the website to share the sound output?"
msgstr "Quere permitir que o sitio web acceda á posición xeográfica?"

#: lib/contents/ui/PermissionQuestion.qml:34
#, fuzzy, kde-format
#| msgid "Do you want to allow the website to access the geo location?"
msgid "Do you want to allow the website to send you notifications?"
msgstr "Quere permitir que o sitio web acceda á posición xeográfica?"

#: lib/contents/ui/PermissionQuestion.qml:43
#, kde-format
msgid "Accept"
msgstr "Aceptar"

#: lib/contents/ui/PermissionQuestion.qml:53
#, kde-format
msgid "Decline"
msgstr "Rexeitar"

#: lib/contents/ui/PrintPreview.qml:24 src/contents/ui/desktop/desktop.qml:269
#, kde-format
msgid "Print"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:67
#, kde-format
msgid "Destination"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:71
#, kde-format
msgid "Save to PDF"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:77
#, kde-format
msgid "Orientation"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:102
#, kde-format
msgid "Paper size"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:148
#, kde-format
msgid "Options"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:152
#, kde-format
msgid "Print backgrounds"
msgstr ""

#: lib/contents/ui/PrintPreview.qml:172
#, kde-format
msgid "Save"
msgstr ""

#: lib/contents/ui/WebView.qml:235
#, kde-format
msgid "Website was opened in a new tab"
msgstr "O sitio web abriuse nun novo separador"

#: lib/contents/ui/WebView.qml:253
#, kde-format
msgid "Entered Full Screen Mode"
msgstr ""

#: lib/contents/ui/WebView.qml:254
#, kde-format
msgid "Exit Full Screen (Esc)"
msgstr ""

#: lib/contents/ui/WebView.qml:387
#, kde-format
msgid "Play"
msgstr ""

#: lib/contents/ui/WebView.qml:388 src/contents/ui/Downloads.qml:47
#, kde-format
msgid "Pause"
msgstr ""

#: lib/contents/ui/WebView.qml:395
#, kde-format
msgid "Unmute"
msgstr ""

#: lib/contents/ui/WebView.qml:396
#, kde-format
msgid "Mute"
msgstr ""

#: lib/contents/ui/WebView.qml:406
#, kde-format
msgid "Speed"
msgstr ""

#: lib/contents/ui/WebView.qml:429
#, kde-format
msgid "Loop"
msgstr ""

#: lib/contents/ui/WebView.qml:436
#, kde-format
msgid "Exit fullscreen"
msgstr ""

#: lib/contents/ui/WebView.qml:436
#, kde-format
msgid "Fullscreen"
msgstr ""

#: lib/contents/ui/WebView.qml:449
#, kde-format
msgid "Hide controls"
msgstr ""

#: lib/contents/ui/WebView.qml:450
#, kde-format
msgid "Show controls"
msgstr ""

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgid "Open video"
msgstr ""

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgid "Open audio"
msgstr ""

#: lib/contents/ui/WebView.qml:459
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open video in new Tab"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:459
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open audio in new Tab"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:471
#, kde-format
msgid "Save video"
msgstr ""

#: lib/contents/ui/WebView.qml:477
#, kde-format
msgid "Copy video Link"
msgstr ""

#: lib/contents/ui/WebView.qml:483
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open image"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:483
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open image in new Tab"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:495
#, fuzzy, kde-format
#| msgid "Share page"
msgid "Save image"
msgstr "Compartir a páxina"

#: lib/contents/ui/WebView.qml:501
#, fuzzy, kde-format
#| msgid "Load images"
msgid "Copy image"
msgstr "Cargar as imaxes"

#: lib/contents/ui/WebView.qml:507
#, kde-format
msgid "Copy image link"
msgstr ""

#: lib/contents/ui/WebView.qml:513
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open link"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:513
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open link in new Tab"
msgstr "Abrir nun novo separador"

#: lib/contents/ui/WebView.qml:525
#, fuzzy, kde-format
#| msgid "Bookmarks"
msgid "Bookmark link"
msgstr "Marcadores"

#: lib/contents/ui/WebView.qml:537
#, kde-format
msgid "Save link"
msgstr ""

#: lib/contents/ui/WebView.qml:543
#, fuzzy, kde-format
#| msgid "Copy Url"
msgid "Copy link"
msgstr "Copiar o URL"

#: lib/contents/ui/WebView.qml:550
#, kde-format
msgid "Copy"
msgstr "Copiar"

#: lib/contents/ui/WebView.qml:556
#, kde-format
msgid "Cut"
msgstr "Cortar"

#: lib/contents/ui/WebView.qml:562
#, kde-format
msgid "Paste"
msgstr "Pegar"

#: lib/contents/ui/WebView.qml:583 src/contents/ui/desktop/desktop.qml:318
#: src/contents/ui/mobile.qml:280
#, kde-format
msgid "Share page"
msgstr "Compartir a páxina"

#: lib/contents/ui/WebView.qml:595
#, fuzzy, kde-format
#| msgid "View source"
msgid "View page source"
msgstr "Ver a fonte"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:14
#, kde-format
msgid ""
"The ad blocker is missing its filter lists, do you want to download them now?"
msgstr ""

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:34
#, fuzzy, kde-format
#| msgid "Download"
msgid "Downloading..."
msgstr "Descargar"

#: src/contents/ui/Bookmarks.qml:12 src/contents/ui/desktop/desktop.qml:248
#: src/contents/ui/mobile.qml:79
#, kde-format
msgid "Bookmarks"
msgstr "Marcadores"

#: src/contents/ui/desktop/BookmarksPage.qml:28
#, kde-format
msgid "Search bookmarks…"
msgstr ""

#: src/contents/ui/desktop/BookmarksPage.qml:70
#, fuzzy, kde-format
#| msgid "Add bookmark"
msgid "No bookmarks yet"
msgstr "Engadir un marcador"

#: src/contents/ui/desktop/desktop.qml:116
#, kde-format
msgid "Search or enter URL…"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:223 src/contents/ui/desktop/Tabs.qml:354
#: src/contents/ui/Tabs.qml:49
#, kde-format
msgid "New Tab"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:230 src/contents/ui/mobile.qml:71
#, kde-format
msgid "Leave private mode"
msgstr "Saír do modo privado"

#: src/contents/ui/desktop/desktop.qml:230 src/contents/ui/mobile.qml:71
#, kde-format
msgid "Private mode"
msgstr "Modo privado"

#: src/contents/ui/desktop/desktop.qml:239 src/contents/ui/History.qml:12
#: src/contents/ui/mobile.qml:87
#, kde-format
msgid "History"
msgstr "Historial"

#: src/contents/ui/desktop/desktop.qml:257 src/contents/ui/Downloads.qml:15
#: src/contents/ui/mobile.qml:91
#, fuzzy, kde-format
#| msgid "Download"
msgid "Downloads"
msgstr "Descargar"

#: src/contents/ui/desktop/desktop.qml:275
#, kde-format
msgid "Full Screen"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:292
#, kde-format
msgid "Hide developer tools"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:293 src/contents/ui/mobile.qml:376
#, kde-format
msgid "Show developer tools"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:302 src/contents/ui/mobile.qml:276
#, kde-format
msgid "Find in page"
msgstr "Atopar na páxina"

#: src/contents/ui/desktop/desktop.qml:311
#, kde-format
msgctxt "@action:inmenu"
msgid "Reader Mode"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:328
#, kde-format
msgid "Add to application launcher"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:347 src/contents/ui/mobile.qml:99
#, kde-format
msgid "Settings"
msgstr "Configuración"

#: src/contents/ui/desktop/desktop.qml:354
#, kde-format
msgid "Configure Angelfish"
msgstr ""

#: src/contents/ui/desktop/HistoryPage.qml:28
#, kde-format
msgid "Search history…"
msgstr ""

#: src/contents/ui/desktop/HistoryPage.qml:44
#, kde-format
msgid "Clear all history"
msgstr ""

#: src/contents/ui/desktop/HistoryPage.qml:80
#, kde-format
msgid "Not history yet"
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:14
#: src/contents/ui/SettingsPage.qml:38
#, kde-format
msgid "Toolbars"
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:28
#, kde-format
msgid "Show home button:"
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:29
#, kde-format
msgid "The home button will be shown next to the reload button in the toolbar."
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:39
#, fuzzy, kde-format
#| msgid "Homepage"
msgid "Homepage:"
msgstr "Páxina inicial"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:61
#, kde-format
msgid "New tabs:"
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:83
#, kde-format
msgid "Always show the tab bar"
msgstr ""

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:84
#, kde-format
msgid "The tab bar will be displayed even if there is only one tab open"
msgstr ""

#: src/contents/ui/desktop/Tabs.qml:219 src/contents/ui/Tabs.qml:185
#, kde-format
msgid "Reader Mode: %1"
msgstr ""

#: src/contents/ui/desktop/Tabs.qml:250 src/contents/ui/Tabs.qml:216
#, kde-format
msgid "Close tab"
msgstr ""

#: src/contents/ui/desktop/Tabs.qml:290
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open a new tab"
msgstr "Abrir nun novo separador"

#: src/contents/ui/desktop/Tabs.qml:323
#, kde-format
msgid "List all tabs"
msgstr ""

#: src/contents/ui/desktop/Tabs.qml:363
#, kde-format
msgid "Reload Tab"
msgstr ""

#: src/contents/ui/desktop/Tabs.qml:371
#, fuzzy, kde-format
#| msgid "Private Tabs"
msgid "Duplicate Tab"
msgstr "Separadores privados"

#: src/contents/ui/desktop/Tabs.qml:379
#, fuzzy, kde-format
#| msgid "Bookmarks"
msgid "Bookmark Tab"
msgstr "Marcadores"

#: src/contents/ui/desktop/Tabs.qml:395
#, kde-format
msgid "Close Tab"
msgstr ""

#: src/contents/ui/Downloads.qml:26
#, kde-format
msgid "No running downloads"
msgstr ""

#: src/contents/ui/Downloads.qml:53
#, kde-format
msgid "Continue"
msgstr ""

#: src/contents/ui/Downloads.qml:91
#, fuzzy, kde-format
#| msgid "Settings"
msgctxt "download state"
msgid "Starting…"
msgstr "Configuración"

#: src/contents/ui/Downloads.qml:93
#, kde-format
msgid "Completed"
msgstr ""

#: src/contents/ui/Downloads.qml:95
#, fuzzy, kde-format
#| msgid "Cancel"
msgid "Cancelled"
msgstr "Cancelar"

#: src/contents/ui/Downloads.qml:97
#, kde-format
msgctxt "download state"
msgid "Interrupted"
msgstr ""

#: src/contents/ui/Downloads.qml:99
#, kde-format
msgctxt "download state"
msgid "In progress"
msgstr ""

#: src/contents/ui/FindInPageBar.qml:45
#, kde-format
msgid "Search..."
msgstr ""

#: src/contents/ui/mobile.qml:18
#, kde-format
msgid "Angelfish Web Browser"
msgstr "Navegador web Angelfish"

#: src/contents/ui/mobile.qml:64 src/contents/ui/Tabs.qml:42
#, kde-format
msgid "Tabs"
msgstr "Separadores"

#: src/contents/ui/mobile.qml:291
#, kde-format
msgid "Add to homescreen"
msgstr ""

#: src/contents/ui/mobile.qml:301
#, fuzzy, kde-format
#| msgid "Open in new Tab"
msgid "Open in app"
msgstr "Abrir nun novo separador"

#: src/contents/ui/mobile.qml:309
#, kde-format
msgid "Go previous"
msgstr "Ir ao anterior"

#: src/contents/ui/mobile.qml:317
#: src/contents/ui/SettingsNavigationBarPage.qml:76
#, kde-format
msgid "Go forward"
msgstr "Ir ao seguinte"

#: src/contents/ui/mobile.qml:324
#, kde-format
msgid "Stop loading"
msgstr "Deixar de cargar"

#: src/contents/ui/mobile.qml:324
#, kde-format
msgid "Refresh"
msgstr "Actualizar"

#: src/contents/ui/mobile.qml:334
#, fuzzy, kde-format
#| msgid "Bookmarks"
msgid "Bookmarked"
msgstr "Marcadores"

#: src/contents/ui/mobile.qml:334
#, fuzzy, kde-format
#| msgid "Bookmarks"
msgid "Bookmark"
msgstr "Marcadores"

#: src/contents/ui/mobile.qml:350
#, kde-format
msgid "Show desktop site"
msgstr "Amosar o sitio de escritorio"

#: src/contents/ui/mobile.qml:359
#, kde-format
msgid "Reader Mode"
msgstr ""

#: src/contents/ui/mobile.qml:367
#, kde-format
msgid "Hide navigation bar"
msgstr "Agochar a barra de navegación"

#: src/contents/ui/mobile.qml:367
#, kde-format
msgid "Show navigation bar"
msgstr "Amosar a barra de navegación"

#: src/contents/ui/NewTabQuestion.qml:11
#, kde-format
msgid ""
"Site wants to open a new tab: \n"
"%1"
msgstr ""
"O sitio quere abrir un separador novo: \n"
"%1"

#: src/contents/ui/NewTabQuestion.qml:19
#, kde-format
msgid "Open"
msgstr "Abrir"

#: src/contents/ui/SettingsAdblock.qml:17
#, kde-format
msgid "Adblock settings"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:28
#, kde-format
msgid "Update lists"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:50
#, kde-format
msgid "The adblock functionality isn't included in this build."
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:56
#, kde-format
msgid "Add filterlist"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:61
#, kde-format
msgid "Name"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:70
#, kde-format
msgid "Url"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:80
#, kde-format
msgid "Add"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:145
#, kde-format
msgid "Remove this filter list"
msgstr ""

#: src/contents/ui/SettingsAdblock.qml:155
#, kde-format
msgid "add Filterlist"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:17 src/contents/ui/SettingsPage.qml:14
#, kde-format
msgid "General"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:40
#, kde-format
msgid "Enable JavaScript"
msgstr "Activar JavaScript"

#: src/contents/ui/SettingsGeneral.qml:41
#, kde-format
msgid "This may be required on certain websites for them to work."
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:50
#, kde-format
msgid "Load images"
msgstr "Cargar as imaxes"

#: src/contents/ui/SettingsGeneral.qml:51
#, kde-format
msgid "Whether to load images on websites."
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:60
#, kde-format
msgid "Enable adblock"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:61
#, kde-format
msgid "Attempts to prevent advertisements on websites from showing."
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:61
#, kde-format
msgid "AdBlock functionality was not included in this build."
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:71
#, kde-format
msgid "Switch to new tab immediately"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:72
#, kde-format
msgid ""
"When you open a link, image or media in a new tab, switch to it immediately"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:81
#, kde-format
msgid "Use Smooth Scrolling"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:82
#, kde-format
msgid ""
"Scrolling is smoother and will not stop suddenly when you stop scrolling. "
"Requires app restart to take effect."
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:91
#, kde-format
msgid "Use dark color scheme"
msgstr ""

#: src/contents/ui/SettingsGeneral.qml:92
#, kde-format
msgid ""
"Websites will have their color schemes set to dark. Requires app restart to "
"take effect."
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:16
#, kde-format
msgid "Navigation bar"
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:36
#, kde-format
msgid ""
"Choose the buttons enabled in navigation bar. Some of the buttons can be "
"hidden only in portrait orientation of the browser and are always shown if  "
"the browser is wider than its height.\n"
"\n"
" Note that if you disable the menu buttons, you will be able to access the "
"menus either by swiping from the left or right side or to a side along the "
"bottom of the window."
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:52
#, kde-format
msgid "Main menu in portrait"
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:58
#, kde-format
msgid "Tabs in portrait"
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:64
#, kde-format
msgid "Context menu in portrait"
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:70
#, kde-format
msgid "Go back"
msgstr ""

#: src/contents/ui/SettingsNavigationBarPage.qml:82
#, kde-format
msgid "Reload/Stop"
msgstr ""

#: src/contents/ui/SettingsPage.qml:20
#, kde-format
msgid "Ad Block"
msgstr ""

#: src/contents/ui/SettingsPage.qml:26 src/contents/ui/SettingsWebApps.qml:16
#, kde-format
msgid "Web Apps"
msgstr ""

#: src/contents/ui/SettingsPage.qml:32
#: src/contents/ui/SettingsSearchEnginePage.qml:16
#: src/contents/ui/SettingsSearchEnginePage.qml:122
#, kde-format
msgid "Search Engine"
msgstr "Motor de busca"

#: src/contents/ui/SettingsSearchEnginePage.qml:32
#, kde-format
msgid "Custom"
msgstr ""

#: src/contents/ui/SettingsSearchEnginePage.qml:123
#, kde-format
msgid "Base URL of your preferred search engine"
msgstr "URL base do seu motor de busca preferido"

#: src/contents/ui/SettingsWebApps.qml:75
#, kde-format
msgid "Remove app"
msgstr ""

#: src/contents/ui/ShareSheet.qml:17
#, fuzzy, kde-format
#| msgid "Share page"
msgid "Share page to"
msgstr "Compartir a páxina"

#: src/main.cpp:95
#, kde-format
msgid "URL to open"
msgstr "URL para abrir"

#, fuzzy
#~| msgid "Private mode"
#~ msgid "Private Mode"
#~ msgstr "Modo privado"

#~ msgid "Search online for '%1'"
#~ msgstr "Buscar «%1» por Internet"

#~ msgid "Search online"
#~ msgstr "Buscar en Internet"

#~ msgid "OK"
#~ msgstr "Aceptar"

#, fuzzy
#~| msgid "Homepage"
#~ msgid "Home"
#~ msgstr "Páxina inicial"

#~ msgid "New"
#~ msgstr "Novo"

#~ msgid "Website that should be loaded on startup"
#~ msgstr "Sitio web que debería cargarse no inicio"

#~ msgid "Find..."
#~ msgstr "Atopar…"

#~ msgid "Highlight text on the current website"
#~ msgstr "Realzar o texto no sitio web actual"

#~ msgid "Start without UI"
#~ msgstr "Iniciar sen interface de usuario"

#~ msgid "Ok"
#~ msgstr "Aceptar"
